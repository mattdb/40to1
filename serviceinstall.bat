@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

if exist D:\Dev\40to1\mysql\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\mysql\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\postgresql\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\postgresql\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\elasticsearch\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\elasticsearch\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\logstash\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\logstash\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\logstash-forwarder\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\logstash-forwarder\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\apache2\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\apache2\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\apache-tomcat\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\apache-tomcat\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\resin\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\resin\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\jboss\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\jboss\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\wildfly\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\wildfly\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\activemq\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\activemq\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\openoffice\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\openoffice\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\subversion\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\subversion\scripts\serviceinstall.bat INSTALL)
rem RUBY_APPLICATION_INSTALL
if exist D:\Dev\40to1\mongodb\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\mongodb\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\lucene\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\lucene\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\third_application\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\third_application\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\nginx\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\nginx\scripts\serviceinstall.bat INSTALL)
if exist D:\Dev\40to1\php\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\php\scripts\serviceinstall.bat INSTALL)
goto end

:remove

if exist D:\Dev\40to1\third_application\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\third_application\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\lucene\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\lucene\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\mongodb\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\mongodb\scripts\serviceinstall.bat)
rem RUBY_APPLICATION_REMOVE
if exist D:\Dev\40to1\subversion\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\subversion\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\openoffice\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\openoffice\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\jboss\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\jboss\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\wildfly\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\wildfly\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\resin\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\resin\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\activemq\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\activemq\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\apache-tomcat\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\apache-tomcat\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\apache2\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\apache2\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\logstash-forwarder\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\logstash-forwarder\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\logstash\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\logstash\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\elasticsearch\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\elasticsearch\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\postgresql\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\postgresql\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\mysql\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\mysql\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\php\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\php\scripts\serviceinstall.bat)
if exist D:\Dev\40to1\nginx\scripts\serviceinstall.bat (start /MIN D:\Dev\40to1\nginx\scripts\serviceinstall.bat)
:end
