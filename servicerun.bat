@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

if exist D:\Dev\40to1\hypersonic\scripts\servicerun.bat (start /MIN D:\Dev\40to1\server\hsql-sample-database\scripts\servicerun.bat START)
if exist D:\Dev\40to1\ingres\scripts\servicerun.bat (start /MIN D:\Dev\40to1\ingres\scripts\servicerun.bat START)
if exist D:\Dev\40to1\mysql\scripts\servicerun.bat (start /MIN D:\Dev\40to1\mysql\scripts\servicerun.bat START)
if exist D:\Dev\40to1\postgresql\scripts\servicerun.bat (start /MIN D:\Dev\40to1\postgresql\scripts\servicerun.bat START)
if exist D:\Dev\40to1\elasticsearch\scripts\servicerun.bat (start /MIN D:\Dev\40to1\elasticsearch\scripts\servicerun.bat START)
if exist D:\Dev\40to1\logstash\scripts\servicerun.bat (start /MIN D:\Dev\40to1\logstash\scripts\servicerun.bat START)
if exist D:\Dev\40to1\logstash-forwarder\scripts\servicerun.bat (start /MIN D:\Dev\40to1\logstash-forwarder\scripts\servicerun.bat START)
if exist D:\Dev\40to1\apache2\scripts\servicerun.bat (start /MIN D:\Dev\40to1\apache2\scripts\servicerun.bat START)
if exist D:\Dev\40to1\openoffice\scripts\servicerun.bat (start /MIN D:\Dev\40to1\openoffice\scripts\servicerun.bat START)
if exist D:\Dev\40to1\apache-tomcat\scripts\servicerun.bat (start /MIN D:\Dev\40to1\apache-tomcat\scripts\servicerun.bat START)
if exist D:\Dev\40to1\resin\scripts\servicerun.bat (start /MIN D:\Dev\40to1\resin\scripts\servicerun.bat START)
if exist D:\Dev\40to1\activemq\scripts\servicerun.bat (start /MIN D:\Dev\40to1\activemq\scripts\servicerun.bat START)
if exist D:\Dev\40to1\jboss\scripts\servicerun.bat (start /MIN D:\Dev\40to1\jboss\scripts\servicerun.bat START)
if exist D:\Dev\40to1\wildfly\scripts\servicerun.bat (start /MIN D:\Dev\40to1\wildfly\scripts\servicerun.bat START)
if exist D:\Dev\40to1\jetty\scripts\servicerun.bat (start /MIN D:\Dev\40to1\jetty\scripts\servicerun.bat START)
if exist D:\Dev\40to1\subversion\scripts\servicerun.bat (start /MIN D:\Dev\40to1\subversion\scripts\servicerun.bat START)
rem RUBY_APPLICATION_START
if exist D:\Dev\40to1\lucene\scripts\servicerun.bat (start /MIN D:\Dev\40to1\lucene\scripts\servicerun.bat START)
if exist D:\Dev\40to1\mongodb\scripts\servicerun.bat (start /MIN D:\Dev\40to1\mongodb\scripts\servicerun.bat START)
if exist D:\Dev\40to1\third_application\scripts\servicerun.bat (start /MIN D:\Dev\40to1\third_application\scripts\servicerun.bat START)
goto end

:stop
echo "Stopping services ..."
if exist D:\Dev\40to1\third_application\scripts\servicerun.bat (start /MIN D:\Dev\40to1\third_application\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\lucene\scripts\servicerun.bat (start /MIN D:\Dev\40to1\lucene\scripts\servicerun.bat STOP)
rem RUBY_APPLICATION_STOP
if exist D:\Dev\40to1\subversion\scripts\servicerun.bat (start /MIN D:\Dev\40to1\subversion\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\jetty\scripts\servicerun.bat (start /MIN D:\Dev\40to1\jetty\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\hypersonic\scripts\servicerun.bat (start /MIN D:\Dev\40to1\server\hsql-sample-database\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\jboss\scripts\servicerun.bat (start /MIN D:\Dev\40to1\jboss\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\wildfly\scripts\servicerun.bat (start /MIN D:\Dev\40to1\wildfly\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\resin\scripts\servicerun.bat (start /MIN D:\Dev\40to1\resin\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\activemq\scripts\servicerun.bat (start /MIN D:\Dev\40to1\activemq\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\apache-tomcat\scripts\servicerun.bat (start /MIN /WAIT D:\Dev\40to1\apache-tomcat\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\openoffice\scripts\servicerun.bat (start /MIN D:\Dev\40to1\openoffice\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\apache2\scripts\servicerun.bat (start /MIN D:\Dev\40to1\apache2\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\logstash-forwarder\scripts\servicerun.bat (start /MIN D:\Dev\40to1\logstash-forwarder\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\logstash\scripts\servicerun.bat (start /MIN D:\Dev\40to1\logstash\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\elasticsearch\scripts\servicerun.bat (start /MIN D:\Dev\40to1\elasticsearch\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\ingres\scripts\servicerun.bat (start /MIN D:\Dev\40to1\ingres\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\mysql\scripts\servicerun.bat (start /MIN D:\Dev\40to1\mysql\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\mongodb\scripts\servicerun.bat (start /MIN D:\Dev\40to1\mongodb\scripts\servicerun.bat STOP)
if exist D:\Dev\40to1\postgresql\scripts\servicerun.bat (start /MIN D:\Dev\40to1\postgresql\scripts\servicerun.bat STOP)

:end
