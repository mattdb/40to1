#!/bin/sh
echo $DYLD_LIBRARY_PATH | egrep "/Applications/40to1/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/Applications/40to1/varnish/bin:/Applications/40to1/sqlite/bin:/Applications/40to1/php/bin:/Applications/40to1/mysql/bin:/Applications/40to1/apache2/bin:/Applications/40to1/common/bin:$PATH"
export PATH
DYLD_LIBRARY_PATH="/Applications/40to1/varnish/lib:/Applications/40to1/varnish/lib/varnish:/Applications/40to1/varnish/lib/varnish/vmods:/Applications/40to1/sqlite/lib:/Applications/40to1/mysql/lib:/Applications/40to1/apache2/lib:/Applications/40to1/common/lib:$DYLD_LIBRARY_PATH"
export DYLD_LIBRARY_PATH
fi

TERMINFO=/Applications/40to1/common/share/terminfo
export TERMINFO
##### VARNISH ENV #####
		
      ##### SQLITE ENV #####
			
SASL_CONF_PATH=/Applications/40to1/common/etc
export SASL_CONF_PATH
SASL_PATH=/Applications/40to1/common/lib/sasl2 
export SASL_PATH
LDAPCONF=/Applications/40to1/common/etc/openldap/ldap.conf
export LDAPCONF
##### PHP ENV #####
		    
##### MYSQL ENV #####

##### APACHE ENV #####

##### CURL ENV #####
CURL_CA_BUNDLE=/Applications/40to1/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/Applications/40to1/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/Applications/40to1/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/Applications/40to1/common/lib/engines
export OPENSSL_ENGINES


. /Applications/40to1/scripts/build-setenv.sh
