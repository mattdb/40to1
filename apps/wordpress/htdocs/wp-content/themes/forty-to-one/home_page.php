<?php
/*
Template Name: Home Page
*/
?>
<?php include 'header.php' ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="hero-container np">
        <?php
        if (has_post_thumbnail()) {
            the_post_thumbnail('full', array('class' => "hero"));
        }
        ?>

        <div class="hero-message">
            <h1><?php echo get_post_custom_values('Hero Title')[0]; ?></h1>
        </div>
    </div>
    <div class="opt-in np">
        <span>GET THE NEWSLETTER</span>

        <form>
            <label for="name">name:</label><input name="name" type="text"/>
            <label for="email">email:</label><input name="email" type="text"/>
            <input type="submit" value="GET IT!"/>
        </form>
    </div>
    <div class="np work-with">
        <div class="profile-pic">
            <?php
            if (class_exists('Dynamic_Featured_Image')) {
                global $dynamic_featured_image;
                $featured_images = $dynamic_featured_image->get_featured_images();
                echo "<img src='" . $featured_images[0]['full'] . "'/>";
            } ?>
        </div>
        <div class="copy">
            <h2>WORK WITH ELLEN!</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque doloribus ducimus earum facere
                magni, quia repellendus. Ex libero natus nemo nihil qui temporibus vero voluptatum? Ad facilis molestiae
                ratione totam.</p>
        </div>
        <a class="learnmore" href="#">LEARN MORE</a>

    </div>
    <div class="container ">
        <div class="row">
            <div class="latest_posts col-xs-12">
                <?php
                $args = array('numberposts' => 3,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_status' => 'publish');
                $recent_posts = wp_get_recent_posts($args);
                foreach ($recent_posts as $recent) {
                    $url = wp_get_attachment_url(get_post_thumbnail_id($recent["ID"]));
                    echo '<div class="col-xs-4" style="background-image: url(' . $url . ')" >';
                    echo '<a href="' . get_permalink($recent["ID"]) . '"></a>';
                    echo  '<div class="data-holder"><h3>'.$recent["post_title"] . '</h3></div>';

                    echo '</div> ';
//            echo  get_the_post_thumbnail( $recent["ID"], "large" );
                }
                ?>
            </div>
        </div>
    </div>

    <?php the_content(); ?>













<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php include 'footer.php' ?>