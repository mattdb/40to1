<div class="col-lg-4" id="sidebar">
<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package forty to one
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
</div></div></div>
