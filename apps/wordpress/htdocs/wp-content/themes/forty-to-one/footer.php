<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package forty to one
 */
?>


</div>
	</div><!-- #content -->

    <div class="row">
        <div class="col-xs-12">
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'forty-to-one' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'forty-to-one' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'forty-to-one' ), 'forty to one', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div></div></div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
