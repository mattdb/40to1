<?php
/**
 * The template for displaying all single posts.
 *
 * @package forty to one
 */

get_header(); ?>

    <div class="row">
        <div class="col-lg-8">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
single
                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('content', 'single'); ?>

                        <?php the_post_navigation(); ?>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;
                        ?>

                    <?php endwhile; // end of the loop. ?>

                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>

    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
