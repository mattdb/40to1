(function ($) {
    var header = $('.header');
    var w = $(window);
    w.scroll(function  () {
        w.scrollTop() > 0 ? header.addClass('darken') : header.removeClass('darken');
    })
}(jQuery));