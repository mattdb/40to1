<?php
/**
 * Created by PhpStorm.
 * User: matthewdelbuono
 * Date: 15-03-24
 * Time: 8:29 PM
 */

function enqueue_custom_scripts(){
//    wp_enqueue_style('handle', 'src');
    wp_enqueue_style('google font', 'http://fonts.googleapis.com/css?family=PT+Serif:400,700|Unna');

  wp_enqueue_script('handle',get_template_directory_uri().'/js/nav.js', array('jquery'), '20140714', true);
//    wp_enqueue_script('handle',get_template_directory_uri().'src', array('dependancies'), 'version', true_in_footer);

}

add_action('wp_enqueue_scripts', 'enqueue_custom_scripts');

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

/* MULTIPLE FEATURED IMAGES */
if (class_exists('MultiPostThumbnails')) {

    new MultiPostThumbnails(array(
        'label' => 'Secondary Image',
        'id' => 'secondary-image',
        'post_type' => 'post'
    ) );
}

/* PREVENT ADMIN BAR FROM SHOWING */
add_filter('show_admin_bar', '__return_false');
