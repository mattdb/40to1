/**
 * Created by matthewdelbuono on 15-03-10.
 */
module.exports = function (grunt) {

    grunt.initConfig({
        watch: {
            options: {
                livereload: true
            },
            php: {
                files: ['wp-content/themes/forty-to-one/**/*.php'],
                tasks: []
            },
            sass: {
                files: ['wp-content/themes/forty-to-one/sass/**/*.scss'],
                tasks: ['compass:dev']
            }
        },
        compass: {
            dev: {
                options: {
                    sassDir: 'wp-content/themes/forty-to-one/sass',
                    cssDir: 'wp-content/themes/forty-to-one',
                    outputStyle: 'expanded',
                    sourcemap: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-open');

    grunt.registerTask('default', ['compass:dev', 'watch']);

};